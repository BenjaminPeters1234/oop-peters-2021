
// AB oop Vererbung, Aufgabe 4.1
// Klasse feiertag erbt von klasse datum

public class Feiertag extends Datum {
	
	private String name;
	
	public Feiertag(){
		super();
		setName("unbekannt");
	}

	public Feiertag(int tag, int monat, int jahr,String name){
		super(tag,monat,jahr);
		setName(name);
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		
		return getTag()+"."+getMonat()+"."+getJahr()+" ("+getName()+")";
	}
}
