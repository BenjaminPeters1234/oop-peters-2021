
// int tag monat jahr, getter setter, konstruktoren, toString, equals

public class Datum {

	private int tag;
	private int monat;
	private int jahr;
	
	// Konstruktor Default, zum initialisieren
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}
	// Konstrukor, mit deklaration von Werten
	public Datum(int tag, int monat, int jahr) {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);		
	}
	
	public int getTag() {
		return tag;
	}
	
	public int getMonat() {
		return monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}
	
	public void setMonat(int monat) {
		this.monat = monat;
	}

	public void setJahr(int Jahr) {
		this.jahr = Jahr;
	}
	
	@Override
	public String toString() {
		
		return "Datum: [ " +tag+"."+monat+"."+jahr+" ]";
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Datum) {
			Datum d = (Datum) obj;
			
			if(this.tag != d.getTag()) return false;
			if(this.monat != d.getMonat()) return false;
			if(this.jahr != d.getJahr()) return false;
			
			return true;
		}
		return false;
		
	}
}
