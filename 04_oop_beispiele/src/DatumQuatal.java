
public class DatumQuatal {

	public static void main(String[] args) {

		Datum d1 = new Datum(01,13,2021);
		Datum d2 = new Datum(01,04,2021);
		Datum d3 = new Datum(01,8,2021);
		Datum d4 = new Datum(01,10,2021);
		
		System.out.println(d1);
		System.out.println(d2);

		System.out.println(quartal(d1));
		System.out.println(quartal(d2));
		System.out.println(quartal(d3));
		System.out.println(quartal(d4));
		
	}

	public static int quartal(Datum d) {

		if (d.getMonat() < 1 || d.getMonat() > 12)
			return 0;

		int quartal = 1;

		if (d.getMonat() > 3 && d.getMonat() <= 6)
			quartal = 2;

		else if (d.getMonat() > 6 && d.getMonat() <= 9)
			quartal = 3;
		
		else if (d.getMonat() > 9 && d.getMonat() <= 12)
			quartal =4;

		return quartal;

	}
}
