
public class Person {

	private String name;				// Deklaration der Variablen in der Klasse
	private int alter;
	private static int anzahl =0;
	
	public Person() {					// konstruktor, damit wird ein objekt initialisiert
		this.name = "Unbekannt";
		this.alter = 0;
		anzahl++;
	}
	
	public Person(String name, int alter) {	// konstruktor, mit deklaration 
		this.name = name;
		this.alter = alter;
		anzahl++;
	}

	public static int getAnzahl() {
		return anzahl;
	}
	
	public String getName() {					//"getter" Methode zum aufrufen von Attributen
		return name;
	}
	
	public void setName(String name) {			//"setter" Methode zum festlegen von Attributen
		this.name = name;
	}
		
	public int getAlter() {
		return alter;
	}
	
	public void setAlter(int alter) {
		this.alter = alter;
	}
	
	@Override
	public String toString() {									//zur Ausgabe des Inhalts der Klasse
		return "[ Name: " + this.name + ", Alter: " + this.alter + " ]";
	}	
	
	@Override	//erkennt wenn es fehler in der "definition" gibt, "equal" ohne s bspw.
	public boolean equals(Object obj) {				//Methode zum verlgiechen von Klassen
		
		if  ( obj instanceof Person) {
			Person p = (Person) obj;
			if(this.name.equals(p.getName()) &&
				this.alter == p.getAlter())
				return true;
			else
				return false;			
		}
		return false;		
	}
	
	
	
}







