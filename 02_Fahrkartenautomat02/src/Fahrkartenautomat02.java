﻿/*
 * Fahrkartenautomat-02, Aufgabe 4: Funktionen werden in Methoden Augelagert
 */


import java.util.Scanner;

class Fahrkartenautomat02
{
    public static void main(String[] args)
    {
       double zuZahlen = fahrkartenbestellungErfassen(); 
       
       double rückgabebetrag = fahrkartenBezahlen(zuZahlen);
       
       fahrkartenAusgeben();
       
       rueckgeldAusgeben(rückgabebetrag); 
    }
    
    // Methoden: double fahrkartenbestellungErfassen(), return zuZahlenderBetrag;
    //			double fahrkartenBezahlen(double zuZahlen), return rückgabebetrag;
    //			void fahrkartenAusgeben()
    //			void rueckgeldAusgeben(double rückgabebetrag)
    
    public static double fahrkartenbestellungErfassen()
    {
        Scanner tastatur = new Scanner(System.in);

        System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();

        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = tastatur.nextInt();

        double zuZahlen = anzahlTickets * zuZahlenderBetrag;
    	
    	return zuZahlen;	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen)
    {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;

    	return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben()
    {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
    public static void rueckgeldAusgeben(double rückgabebetrag)
    {
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro ",rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

     	   int CTrückgabebetrag = (int) (rückgabebetrag*100);
     	   
            while(CTrückgabebetrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          CTrückgabebetrag -= 200;
            }
            while(CTrückgabebetrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          CTrückgabebetrag -= 100;
            }
            while(CTrückgabebetrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          CTrückgabebetrag -= 50;
            }
            while(CTrückgabebetrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          CTrückgabebetrag -= 20;
            }
            while(CTrückgabebetrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          CTrückgabebetrag -= 10;
            }
            while(CTrückgabebetrag >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          CTrückgabebetrag -= 5;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    
}