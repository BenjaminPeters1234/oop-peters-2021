
public class Czahl {

	private double real;
	private double imag;
	
	public Czahl() {
		this.real =0.0;
		this.imag =0.0;
	}
	
	public Czahl(double real, double imag) {
		this.real = real;
		this.imag = imag;
	}
	
	public double getReal() {
		return real;
	}
	
	public double getImag()	{
		return imag;
	}
	
	public void setReal(double real) {
		this.real = real;
	}
	
	public void setImag(double imag) {
		this.imag = imag;
	}
	
	@Override
	public String toString() {
		return "Zahl="+real+"+j*("+imag+")";
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Czahl) {
			Czahl z = (Czahl) obj;
			
			if(this.real != z.getReal() || this.imag != z.getImag())
				return false;
			
			return true;
		}
		return false;
	}
	
	public static Czahl multiply(Czahl z1, Czahl z2) {		//Klassenmethode
		
		Czahl z3 = new Czahl();	//re=re1*re2-im1*im2, im=(re1*im2+im2*re1)
		
		z3.setReal( z1.getReal()*z2.getReal()-z1.getImag()*z2.getImag() );
		z3.setImag( z1.getReal()*z2.getImag()-z2.getReal()*z1.getImag() );
		
		return z3;
	}

	public Czahl multiply(Czahl z2) {		//Objektmethde
		
		Czahl z3 = new Czahl();
		
		z3.setReal( this.getReal()*z2.getReal()-this.getImag()*z2.getImag());
		z3.setImag( this.getReal()*z2.getImag()-z2.getReal()*this.getImag());
		
		return z3;
	}


	
}
